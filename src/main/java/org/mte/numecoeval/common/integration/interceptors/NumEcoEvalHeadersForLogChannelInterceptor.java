package org.mte.numecoeval.common.integration.interceptors;

import org.slf4j.MDC;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.ChannelInterceptor;

/**
 * {@link ChannelInterceptor} pour NumEcoEval se basant sur les headers dans les messages pour alimenter
 * le {@link MDC} via les headers des messages échangés.
 */
public class NumEcoEvalHeadersForLogChannelInterceptor implements ChannelInterceptor {

    public static final String HEADER_NOM_LOT = "nomLot";
    public static final String HEADER_DATELOT = "dateLot";
    public static final String HEADER_NOM_ORGANISATION = "nomOrganisation";

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        initMDC(message);
        return ChannelInterceptor.super.preSend(message, channel);
    }

    @Override
    public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {
        ChannelInterceptor.super.afterSendCompletion(message, channel, sent, ex);
        MDC.clear();
    }

    @Override
    public Message<?> postReceive(Message<?> message, MessageChannel channel) {
        initMDC(message);
        return ChannelInterceptor.super.postReceive(message, channel);
    }

    @Override
    public void afterReceiveCompletion(Message<?> message, MessageChannel channel, Exception ex) {
        ChannelInterceptor.super.afterReceiveCompletion(message, channel, ex);
        MDC.clear();
    }

    private static void initMDC(Message<?> message) {
        if(message.getHeaders().containsKey(HEADER_NOM_LOT)) {
            MDC.put(HEADER_NOM_LOT, (String) message.getHeaders().get(HEADER_NOM_LOT));
        }
        if(message.getHeaders().containsKey(HEADER_DATELOT)) {
            MDC.put(HEADER_DATELOT, (String) message.getHeaders().get(HEADER_DATELOT));
        }
        if(message.getHeaders().containsKey(HEADER_NOM_ORGANISATION)) {
            MDC.put(HEADER_NOM_ORGANISATION, (String) message.getHeaders().get(HEADER_NOM_ORGANISATION));
        }
    }


}
