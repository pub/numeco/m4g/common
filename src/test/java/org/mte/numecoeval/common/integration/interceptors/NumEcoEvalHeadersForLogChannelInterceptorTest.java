package org.mte.numecoeval.common.integration.interceptors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.common.exception.NumEcoEvalRuntimeException;
import org.slf4j.MDC;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

class NumEcoEvalHeadersForLogChannelInterceptorTest {

    NumEcoEvalHeadersForLogChannelInterceptor channelInterceptor = new NumEcoEvalHeadersForLogChannelInterceptor();

    @Mock
    Message message;

    @Mock
    MessageChannel channel;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void whenHeadersUnavailable_preSend_shouldNotChangeMDC() {
        MessageHeaders messageHeaders = new MessageHeaders(
                Map.of()
        );
        when(message.getHeaders()).thenReturn(messageHeaders);

        channelInterceptor.preSend(message, channel);

        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT));
        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT));
        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION));
    }

    @Test
    void whenHeadersAvailable_preSend_shouldAddThemToTheMDC() {
        String valueDateLot = "2022-02-01";
        String valueNomOrganisation = "TEST";
        String valueNomLot = valueNomOrganisation + "|" + valueDateLot;
        MessageHeaders messageHeaders = new MessageHeaders(
                Map.of(
                        NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT, valueNomLot,
                        NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT, valueDateLot,
                        NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION, valueNomOrganisation
                )
        );
        when(message.getHeaders()).thenReturn(messageHeaders);

        channelInterceptor.preSend(message, channel);

        assertEquals(valueDateLot, MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT));
        assertEquals(valueNomOrganisation, MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION));
    }

    @Test
    void whenHeadersUnavailable_postReceive_shouldNotChangeMDC() {
        MessageHeaders messageHeaders = new MessageHeaders(
                Map.of()
        );
        when(message.getHeaders()).thenReturn(messageHeaders);

        channelInterceptor.postReceive(message, channel);

        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT));
        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT));
        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION));
    }

    @Test
    void whenHeadersAvailable_postReceive_shouldAddThemToTheMDC() {
        String valueDateLot = "2022-02-01";
        String valueNomOrganisation = "TEST";
        String valueNomLot = valueNomOrganisation + "|" + valueDateLot;
        MessageHeaders messageHeaders = new MessageHeaders(
                Map.of(
                        NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT, valueNomLot,
                        NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT, valueDateLot,
                        NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION, valueNomOrganisation
                )
        );
        when(message.getHeaders()).thenReturn(messageHeaders);

        channelInterceptor.postReceive(message, channel);

        assertEquals(valueNomLot, MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT));
        assertEquals(valueDateLot, MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT));
        assertEquals(valueNomOrganisation, MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION));
    }

    @Test
    void afterSendCompletion_shouldClearMDC() {
        String valueDateLot = "2022-02-01";
        String valueNomOrganisation = "TEST";
        String valueNomLot = valueNomOrganisation + "|" + valueDateLot;
        MDC.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT, valueNomLot);
        MDC.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT, valueDateLot);
        MDC.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION, valueNomOrganisation);

        channelInterceptor.afterSendCompletion(message, channel, true, new NumEcoEvalRuntimeException("Test"));

        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT));
        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT));
        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION));
    }

    @Test
    void afterReceiveCompletion_shouldClearMDC() {
        String valueDateLot = "2022-02-01";
        String valueNomOrganisation = "TEST";
        String valueNomLot = valueNomOrganisation + "|" + valueDateLot;
        MDC.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT, valueNomLot);
        MDC.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT, valueDateLot);
        MDC.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION, valueNomOrganisation);

        channelInterceptor.afterReceiveCompletion(message, channel, new NumEcoEvalRuntimeException("Test"));

        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT));
        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT));
        assertNull(MDC.get(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION));
    }
}
